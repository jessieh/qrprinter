-- spec/encode_string_spec.lua
-- Specifies tests for qrprinter.encode_string functionality

--------------------------------------------------------------------------------
-- Disable LuaJIT compiler

-- LuaJIT's optimizations can frob luacov's coverage reports
require( 'jit' ).off()

--------------------------------------------------------------------------------
-- Module imports

qrprinter = require( 'qrprinter' )

--------------------------------------------------------------------------------
-- Test context

describe( 'qrprinter.encode_string functionality', function ()

             ----------------------------------------
             -- Test definitions

             it( 'returns nil for empty strings', function()

                    local expected_value = nil

                    local empty_string = ''

                    assert.are.same( expected_value, qrprinter.encode_string( empty_string ) )

             end )

             it( 'encodes alphanumeric strings', function()

                    local expected_value = {
                       { true, true, true, true, true, true, true, false, false, true, false, false, true, false, true, true, true, true, true, true, true },
                       { true, false, false, false, false, false, true, false, true, false, false, true, false, false, true, false, false, false, false, false, true },
                       { true, false, true, true, true, false, true, false, false, true, false, false, false, false, true, false, true, true, true, false, true },
                       { true, false, true, true, true, false, true, false, true, false, false, true, false, false, true, false, true, true, true, false, true },
                       { true, false, true, true, true, false, true, false, false, false, true, true, true, false, true, false, true, true, true, false, true },
                       { true, false, false, false, false, false, true, false, true, true, true, false, true, false, true, false, false, false, false, false, true },
                       { true, true, true, true, true, true, true, false, true, false, true, false, true, false, true, true, true, true, true, true, true },
                       { false, false, false, false, false, false, false, false, false, false, true, true, true, false, false, false, false, false, false, false, false },
                       { true, true, true, true, true, false, true, true, true, true, false, false, true, true, false, true, false, true, false, true, false },
                       { false, false, true, true, false, false, false, false, false, true, false, false, true, false, false, true, false, false, true, false, true },
                       { true, false, false, false, false, true, true, false, false, false, true, true, false, true, false, false, true, false, false, true, false },
                       { true, true, true, true, true, false, false, false, false, true, false, false, false, false, false, true, true, true, true, true, false },
                       { true, false, false, false, true, true, true, true, false, false, true, true, false, true, false, false, true, false, false, false, false },
                       { false, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true, false, false, true, false, true },
                       { true, true, true, true, true, true, true, false, true, true, false, false, true, false, true, true, false, true, false, true, false },
                       { true, false, false, false, false, false, true, false, false, false, true, true, true, true, true, false, true, false, true, false, true },
                       { true, false, true, true, true, false, true, false, true, false, true, false, true, false, false, true, false, true, false, true, false },
                       { true, false, true, true, true, false, true, false, true, true, false, false, true, false, false, false, true, false, true, false, false },
                       { true, false, true, true, true, false, true, false, true, false, true, true, false, true, false, true, false, true, true, false, false },
                       { true, false, false, false, false, false, true, false, true, false, true, false, false, false, false, true, true, false, true, false, false },
                       { true, true, true, true, true, true, true, false, true, true, true, true, false, true, false, true, false, true, false, true, false }
                    }

                    local test_string = 'abc123'

                    assert.are.same( expected_value, qrprinter.encode_string( test_string ) )

             end )

             it( 'encodes UTF-8 strings', function()

                    local expected_value = {
                       { true, true, true, true, true, true, true, false, false, false, true, false, true, false, true, true, true, true, true, true, true },
                       { true, false, false, false, false, false, true, false, false, false, false, false, true, false, true, false, false, false, false, false, true },
                       { true, false, true, true, true, false, true, false, true, false, true, false, false, false, true, false, true, true, true, false, true },
                       { true, false, true, true, true, false, true, false, false, false, false, false, true, false, true, false, true, true, true, false, true },
                       { true, false, true, true, true, false, true, false, false, true, false, true, true, false, true, false, true, true, true, false, true },
                       { true, false, false, false, false, false, true, false, false, true, true, true, false, false, true, false, false, false, false, false, true },
                       { true, true, true, true, true, true, true, false, true, false, true, false, true, false, true, true, true, true, true, true, true },
                       { false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false },
                       { true, true, true, false, true, true, true, true, true, false, true, false, true, true, true, false, false, false, true, false, false },
                       { false, false, true, false, false, true, false, true, false, true, false, true, false, false, false, false, true, false, true, false, false },
                       { false, true, true, false, true, false, true, false, true, true, false, true, false, true, true, false, false, true, false, false, false },
                       { false, false, true, false, false, false, false, true, true, true, true, true, true, false, true, true, false, true, true, true, false },
                       { true, true, true, false, true, true, true, false, false, false, false, true, false, true, false, true, true, true, true, false, true },
                       { false, false, false, false, false, false, false, false, true, true, false, true, false, true, false, false, false, true, false, false, true },
                       { true, true, true, true, true, true, true, false, true, true, true, false, true, false, true, false, false, false, false, true, false },
                       { true, false, false, false, false, false, true, false, true, true, false, false, true, true, true, false, true, true, true, true, false },
                       { true, false, true, true, true, false, true, false, true, false, false, true, true, true, true, true, false, true, true, true, false },
                       { true, false, true, true, true, false, true, false, false, true, false, false, true, false, true, false, false, true, true, true, false },
                       { true, false, true, true, true, false, true, false, true, false, true, false, false, true, false, false, false, true, true, false, true },
                       { true, false, false, false, false, false, true, false, true, true, false, true, false, true, true, true, false, false, true, true, false },
                       { true, true, true, true, true, true, true, false, true, true, true, true, true, true, true, false, true, true, false, true, true }
                    }

                    local test_string = '💙🔵🔷'

                    assert.are.same( expected_value, qrprinter.encode_string( test_string ) )

             end )

end )
