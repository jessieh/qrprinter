# <img src="../../raw/assets/icon.png" width=75> qrprinter

A wrapper for `libqrencode` that provides an easy way of encoding and printing strings as QR codes from Lua.

**This uses LuaJIT's FFI library, and so will not run in PUC Lua implementations.**

Written in [Fennel](https://fennel-lang.org).

[![Test Status](https://gitlab.com/jessieh/qrprinter/badges/main/pipeline.svg)](https://gitlab.com/jessieh/qrprinter/-/pipelines) [![Code Coverage](https://gitlab.com/jessieh/qrprinter/badges/main/coverage.svg)](https://gitlab.com/jessieh/qrprinter/-/jobs)

## Table of Contents

 - [Installation](#installation)
 - [Example Usage](#example-usage)
 - [Documentation](#documentation)
   - [Encoding](#encoding)
     - [qrprinter.encode_string](#qrprinterencode_string)
   - [Printing](#config-structure)
     - [qrprinter.print_qr](#qrprinterprint_qr)
     - [qrprinter.print_qr_ascii](#qrprinterprint_qr_ascii)
     - [qrprinter.print_qr_ansi](#qrprinterprint_qr_ansi)
     - [qrprinter.print_qr_utf8](#qrprinterprint_qr_utf8)
 - [Testing](#testing)

## Installation

Install dependencies:

```bash
# Debian/Ubuntu:
apt install libqrencode-dev

# Alpine:
apk add gcompat libqrencode-dev
```

Install with [luarocks](https://luarocks.org):

```bash
luarocks install qrprinter
```

For a list of available versions, see the [luarocks module page](https://luarocks.org/modules/jessieh/qrprinter).

## Example Usage

<img src="../../raw/assets/demo.png" width=500>

## Documentation

### Encoding

#### qrprinter.encode_string
```
qrprinter.encode_string( string )
```
Encodes `string` into a table representing a QR code.\
Returns a two-dimensional sequential table of true/false values representing black/white QR modules.

### Printing

All print functions accept an optional table `options` that may contain two keys:
* `invert`: If non-nil, the output colors will be inverted
* `padding`: Number of blanks cells to print around the QR code (default 2)

#### qrprinter.print_qr
```
qrprinter.print_qr( qr, [options] )
```
Print `qr` to `stdout` using ANSI escape sequences.\
Alias of `qrprinter.print_qr_ansi`.

#### qrprinter.print_qr_ascii
```
qrprinter.print_qr_ascii( qr, [options] )
```
Print `qr` to `stdout` using ASCII characters.

#### qrprinter.print_qr_ansi
```
qrprinter.print_qr_ansi( qr, [options] )
```
Print `qr` to `stdout` using ANSI escape sequences.

#### qrprinter.print_qr_utf8
```
qrprinter.print_qr_utf8( qr, [options] )
```
Print `qr` to `stdout` using UTF8 block elements.

## Testing

The Fennel code needs to be compiled before tests can be run:

```bash
# Build the project:
luarocks build

# Now you can run tests:
luarocks test
```
